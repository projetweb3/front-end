FROM nginx

RUN apt update && apt upgrade -y

COPY . /var/www/html/
COPY nginx.conf /etc/nginx/nginx.conf