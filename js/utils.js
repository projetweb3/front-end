//From https://www.w3schools.com/js/js_cookies.asp
function getCookie(cookieName) {
    var name = cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Fetch the html from the path, and inject it the corresponding id
function loadComponent(path, id, replaceWith = false) {
    if (replaceWith) {
        $.get("component/" + path + ".html", function(data) {
            $(id).replaceWith(data);
        })
    } else {
        $.get("component/" + path + ".html", function(data) {
            $(id).html(data);
        })
    }
}

// Populate the #info html component and set its visibility to true
function displayInfo(msg) {
    $('#info').html(msg);
    $('#info').removeAttr('hidden');
  }