var user = {};

// function that check if the user is already authenticated, and display the right menu relative to the user status (admin, member, ...)
$(function() {
    fetch('api/auth/authenticated')
      .then(response => {
        return response.json();
      })
      .then(response => {
        if (response['authenticated'] != false) {
          user = response;
        } else {
          user['status'] = "";
        }
        
        displayMenuOptionsAndHome();
      })
})

// Display the menu relative to the user status
function displayMenuOptionsAndHome() {
  let userMenuId = "#userMenu";
  let userOptionsId = "#userOptions";
  switch(user['status']) {
    case 1:
      loadComponent('userOptions/index', userOptionsId);
      loadComponent('userMenu/admin', userMenuId, true);
      loadComponent('home/admin', '#mainContent');
      break;
    case 2:
      loadComponent('userOptions/index', userOptionsId);
      loadComponent('userMenu/member', userMenuId, true);
      loadComponent('home/index', '#mainContent');
      break;
    default:
      loadComponent("userOptions/visitor", userOptionsId);
      loadComponent('userMenu/visitor', userMenuId, true);
      loadComponent('home/index', '#mainContent');
  }
}