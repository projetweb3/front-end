user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    server {
        listen 80;
        listen [::]:80;

        server_name acourtin.fr;

        return 301 https://$server_name$request_uri;
    }
    
    server {
        listen      443 ssl;
        listen      [::]:443 ssl;
        
        server_name acourtin.fr;

        root    /var/www/html;
        index   index.html;

        ssl_certificate /etc/letsencrypt/live/acourtin.fr/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/acourtin.fr/privkey.pem;

        location / {
            try_files $uri $uri/ =404;
        }

        location /api/ {
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

            rewrite /api/(.*) /$1 break;
            proxy_pass http://mybroker-gunicorn:5000;
        }
        
    }
}
